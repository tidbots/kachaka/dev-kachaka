# Copyright (c) 2024 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
#FROM nvidia/opengl:base-ubuntu22.04 
#FROM nvidia/opengl:1.0-glvnd-devel-ubuntu22.04
FROM nvcr.io/nvidia/cuda:11.7.1-cudnn8-devel-ubuntu22.04
LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="July 11, 2023"

SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics
ENV __NV_PRIME_RENDER_OFFLOAD=1
ENV __GLX_VENDOR_LIBRARY_NAME=nvidia
ARG ROS_DISTRO=humble

# Timezone, Launguage
RUN apt update \
  && apt install -y --no-install-recommends \
     locales \
     language-pack-ja-base language-pack-ja \
     software-properties-common tzdata \
     fonts-ipafont fonts-ipaexfont fonts-takao
RUN  locale-gen ja_JP ja_JP.UTF-8  \
  && update-locale LC_ALL=ja_JP.UTF-8 LANG=ja_JP.UTF-8 \
  && add-apt-repository universe
# Locale
ENV LANG ja_JP.UTF-8
ENV TZ=Asia/Tokyo


# install packages
RUN apt-get update && apt-get install -q -y --no-install-recommends \
    curl sudo \
    gnupg \
    lsb-release git \
  && rm -rf /var/lib/apt/lists/*

# install ROS2 humble
ENV UBUNTU_CODENAME=focal
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - \
    && echo "deb http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list
RUN apt-get update && apt-get install -y \
    ros-humble-desktop-full  \ 
    ros-humble-compressed-depth-image-transport  \
    python3-colcon-common-extensions \
    ros-humble-rqt-* \
#    gazebo ros-humble-gazebo-*  \
    && rm -rf /var/lib/apt/lists/*

# Install tmux 3.2 (if you wish)
RUN apt-get update && apt-get install -y automake autoconf pkg-config libevent-dev libncurses5-dev bison && \
apt-get clean && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/tmux/tmux.git && \
cd tmux && git checkout tags/3.2 && ls -la && sh autogen.sh && ./configure && make -j8 && make install

# Add user and group
ARG UID
ARG GID
ARG USER_NAME
ARG GROUP_NAME
ARG PASSWORD

RUN groupadd -g $GID $GROUP_NAME && \
    useradd -m -s /bin/bash -u $UID -g $GID -G sudo $USER_NAME && \
    echo $USER_NAME:$PASSWORD | chpasswd && \
    echo "$USER_NAME   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USER_NAME}

RUN sudo apt-get update && sudo apt-get install -q -y --no-install-recommends \
  git wget \
  x11-utils x11-apps terminator xterm xauth \
  terminator xterm nano vim htop \
  python3-rosdep  \
  && sudo rm -rf /var/lib/apt/lists/*
RUN sudo rosdep init && rosdep update

RUN cd ~/ &&  \
    git config --global user.email "you@example.con" &&  \
    git config --global user.name "Your Name"  && \
    git clone https://github.com/pf-robotics/kachaka-api.git

# pf-robotics
RUN set -x &&  \
    cd ~/ &&  \
    mkdir -p ~/kachaka_ws/src &&  \
    cd ~/kachaka_ws/src &&  \
    ln -s ~/kachaka-api/ros2/kachaka_interfaces/ kachaka_interfaces && \  
    ln -s ~/kachaka-api/ros2/kachaka_description/ kachaka_description && \
    ln -s ~/kachaka-api/ros2/demos/kachaka_bringup/ kachaka_bringup && \
    ln -s ~/kachaka-api/ros2/demos/kachaka_bringup/ kachaka_nav2_bringup && \
    ln -s ~/kachaka-api/ros2/demos/kachaka_follow/ kachaka_follow && \
    ln -s ~/kachaka-api/ros2/demos/kachaka_speak/ kachaka_speak && \
    ln -s ~/kachaka-api/ros2/demos/kachaka_vision/ kachaka_vision && \
    ln -s ~/kachaka-api/ros2/demos/kachaka_smart_speaker/ kachaka_smart_speaker && \
    sudo apt-get update && \
    rosdep install -y -i --from-paths .  && \
    cd ~/kachaka_ws && \
    /bin/bash -c "source /opt/ros/humble/setup.sh; colcon build"


# sometimes the following link is not available, then please see and find the mirror link in https://github.com/CMU-Perceptual-Computing-Lab/openpose/issues/1567
RUN cd ~/kachaka-api/ros2/demos/kachaka_vision/config &&  \
#    wget http://posefs1.perception.cs.cmu.edu/OpenPose/models/hand/pose_iter_102000.caffemodel &&  \
    wget https://www.dropbox.com/s/gqgsme6sgoo0zxf/pose_iter_102000.caffemodel  &&  \
    wget https://raw.githubusercontent.com/CMU-Perceptual-Computing-Lab/openpose/master/models/hand/pose_deploy.prototxt &&  \
    cd ~/kachaka_ws && \
    /bin/bash -c "source /opt/ros/humble/setup.sh; colcon build"




# Config (if you wish)
RUN mkdir -p ~/.config/terminator/
COPY assets/terminator_config /home/$USER_NAME/.config/terminator/config 

COPY assets/entrypoint.sh /entrypoint.sh
RUN echo "source /opt/ros/humble/setup.bash" >> ~/.bashrc
RUN echo "source ~/kachaka_ws/install/setup.bash" >> ~/.bashrc
#RUN echo "source ~/ros2_ws/install/setup.bash" >> ~/.bashrc

# entrypoint
COPY ./assets/entrypoint.sh /
RUN sudo chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
#CMD ["/bin/bash"]
CMD ["terminator"]

